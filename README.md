# Checkout payment process
elmenus task

## Introduction
A REST API that simulates an e-commerce checkout process.


## Application Guide:

1. "git clone" repository on your device
```
git clone https://gitlab.com/AmirHaytham/elmenus.git
cd elmenus
```

2. run "docker-compose up -d" inside cloned repository
3. Postman APIs collection && ERD Diagram can be found "/nodejs-server/docs"
4. nodejs-server-dev is the main-app, it waits for mysql to be up, then the main-app will appear in docker hub

## Why I choose Node Js?

There are many concurrent requests that hits the server, the requests are mainly I/O operations which do not require many computational power that may block the event loop. For these two reasons, I have chosen node js as node is very light weight and execute fast. Also, node works with the event loop which is single threaded that preforms all I/O operations asynchronously without blocking the remaning code, this make it preform very fast in handling too many requests at the same time.

## Why I choose sql Database rather NoSql?

SQL databases are efficient at processing queries and joining data across tables, making it easier to perform complex queries against structured data, including ad hoc requests. NoSQL databases lack consistency across products and typically require more work to query data, particular as query complexity increases.

## Docker-compose file:

There are 4 services that should be up and running:

1. nodejs-server-dev --> The main application
2. mysql-database-dev --> Database used with main app
3. mysql-adminer --> Admin portal for database @localhost:8080
4. nodejs-server-test --> Service that runs unit/integration tests

## Open database admin portal:

1. Go to "localhost:8080" on browser
2. Enter the following:
   System: MySQL
   Server: mysql-database
   Username: root
   Password: amirsalama1999
   Database: test_db

###### Process Flow:

- External request received --> routers --> schemas (input parameters validation) -->
- controllers --> validators (execute business logic validations)
- controllers --> adapters --> database

## Project Structure:

- nodejs-server is the node application
- config directory --> catch exported environment variables to be used for project setup
- database directory --> setup "Sequelize" orm
- docs directory --> contains project documentation (ERD diagram & Postman APIs collection)
- errors directory --> defining errors needed in the project
- models directory --> database models
- server directory --> the source code for the project

###### Server Directory:

- adapters: any calls to the external should be executed here (database calls)
- validators: validate business logic (Fraud + Validation Rules set by business)
- controllers: contains business logic
- routers: routes entrypoint (route request to be processed)
- schemas: Validation on input payload received from and call made to this application
- startup: initialising express application and adding all express middlewares
- index.js: server creation
